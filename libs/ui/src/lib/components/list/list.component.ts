import { Component, OnInit } from '@angular/core';
import { PostService } from '../../services/post/post.service';
import { Post } from '../../interfaces/post';
import * as moment from 'moment';

@Component({
  selector: 'reign-challenge-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  posts:Post[] = [];

  constructor(private postService: PostService) { }
  
  ngOnInit(): void {
    this.getPosts();
  }

  getPosts(): void {
    let posts_tmp = [], created_at_tmp, today, difference;
    this.postService.getPost().subscribe((response)=>{
      posts_tmp = response.data;
      posts_tmp.forEach(value => {
        created_at_tmp = moment(value.created_at);
        today = moment();
        difference = today.diff(created_at_tmp, 'days');
        switch (difference) {
          case 0:
            value.created_at = created_at_tmp.format('LT');
            break;
          case 1:
            value.created_at = 'Yesterday';
            break;
          default:
            value.created_at = created_at_tmp.format('DD MMMM');
            break;
        }
      });
      this.posts = posts_tmp;
    });  
  }

  openLink(url:string, event:Event): void{
    if((event.target as Element).className!=='delete')
      window.open(url, "_blank");
  }

  deleteRecord(_id:string): void{
    this.postService.deletePost(_id).subscribe((response)=>{
      if(response.success)
        this.posts = this.posts.filter((value:Post) => value._id !== _id);
    });
  }

}
