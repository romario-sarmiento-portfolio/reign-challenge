import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Route } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { ListComponent } from './components/list/list.component';

export const uiRoutes: Route[] = [
  { path: '', component: HomeComponent }
];

@NgModule({
  imports: [CommonModule, RouterModule, HttpClientModule],
  declarations: [
    HomeComponent,
    HeaderComponent,
    ListComponent
  ],
})
export class UiModule {}
