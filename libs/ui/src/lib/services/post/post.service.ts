import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Post } from '../../interfaces/post';
import { Response } from '../../interfaces/response';
import { environment } from '../../../../../../apps/client/src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(
    private http: HttpClient
  ) { }

  getPost(){
    const path = `${environment.API_URL}/api/post`;
    return this.http.get<Response<Post[]>>(path);
  }

  deletePost(_id:string){
    const httpParams = new HttpParams().set('id', _id);
    const options = { params: httpParams };
    const path = `${environment.API_URL}/api/post`;
    return this.http.delete<Response<Post[]>>(path, options);
  }
}
