# Reign Challenge

For start your local development, you have to install dependencies with:
- `npm install`
Later, you have to install globally "nx" for add new components, services or any other feature to monorepo:
- `npm i -g nx`
Now, you can run your local environment using:
- `npm run serve:client` or `nx serve client`
- `npm run serve:server` or `nx serve server`
Add "--configuration=production" for run your app in production mode:
- `nx serve <app-name> --configuration=production`

If you want to mount docker images:
- `docker-compose up --build`

Previously, you have to install docker-compose. You can find a guide about that in: https://docs.docker.com/compose/install/.

You can change environment variables for your applications in "docker-compose.yml" and select your desired ports:
```
  environment:
    - MONGO_DB_HOST=mongodb
    - ALLOWED_HOST=http://localhost:5000
    - SCHEDULE_MINUTES=30
  ports:
    - local_port:docker_port
```

#### Database - MongoDB

For populate your database, you can change the minute for sync data from "https://hn.algolia.com/api/v1/search_by_date?query=nodejs", because the minute was setted as environment variable (Note: For each hour, your database will sync with "Hacker News" posts):

- `- SCHEDULE_MINUTES=30`

Choose the nearest hour.

Or you have another option, you can import a JSON for posts collection included in db folder.
