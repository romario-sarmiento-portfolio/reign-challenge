import { Component } from '@angular/core';

@Component({
  selector: 'reign-challenge-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'Reign - Client';
}
