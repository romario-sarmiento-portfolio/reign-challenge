import { Controller, Query, Get, Delete } from '@nestjs/common';

import { PostService } from '../services/post.service';

@Controller('post')
export class PostController {
  constructor(private readonly postService: PostService) {}

  @Get()
  async getData() {
    const data = await this.postService.getData();
    return {success: true, data: data};
  }

  @Delete()
  async deleteData(@Query() query) {
    await this.postService.deleteData(query.id);
    return {success: true};
  }
}
