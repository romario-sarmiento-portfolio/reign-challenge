import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';
import { PostController } from '../controllers/post.controller';
import { PostService } from '../services/post.service';
import { Post, PostSchema } from '../schemas/post.schema';

describe('PostController', () => {
  let app: TestingModule;
  let service: PostService;
  let controller: PostController;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [PostController],
      providers: [PostService],
      imports: [
        MongooseModule.forRoot(`mongodb://${process.env.MONGO_DB_HOST}:27017/reign-challenge`),
        MongooseModule.forFeature([{ name: Post.name, schema: PostSchema }])
      ]
    }).compile();
    service = app.get<PostService>(PostService);
    controller = app.get<PostController>(PostController);
  });
  describe('getData', () => {
    it('response', async () => {
      const result:any = [{url:null,story_url:"https://www.economist.com/finance-and-economics/2021/10/10/credit-card...",created_at:"2021-10-12T02:24:02.000Z",author:"elzbardico",story_title:"Credit-card firms are becoming reluctant regulators of the web",title:null}];
      jest.spyOn(service, 'getData').mockImplementation(() => result);

      expect(await controller.getData()).toEqual({"data": result, "success": true});
    });
  });
});
