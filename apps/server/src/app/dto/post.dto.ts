export class CreatePost {
    _id: string;
    title: string;
    story_title: string;
    author: string;
    created_at: string;
    story_url: string;
    url: string;
 }