import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { HttpModule } from '@nestjs/axios';

import { AppController } from './app.controller';
import { AppService } from './app.service';

import { PostController } from './controllers/post.controller';
import { PostService } from './services/post.service';

import { SyncPostsService } from './tasks/sync-posts.service';

import { Post, PostSchema } from './schemas/post.schema';
@Module({
  imports: [
    MongooseModule.forRoot(`mongodb://${process.env.MONGO_DB_HOST}:27017/reign-challenge`),
    MongooseModule.forFeature([{ name: Post.name, schema: PostSchema }]),
    ScheduleModule.forRoot(),
    HttpModule
  ],
  controllers: [AppController, PostController],
  providers: [AppService, PostService, SyncPostsService],
})
export class AppModule {}
