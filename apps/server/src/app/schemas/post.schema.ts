import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PostDocument = Post & Document;

@Schema(/*{collection: 'posts'}*/)
export class Post {

  @Prop()
  title: string;

  @Prop()
  story_title: string;

  @Prop()
  author: string;

  @Prop()
  created_at: string;

  @Prop()
  story_url: string;

  @Prop()
  url: string;
}

export const PostSchema = SchemaFactory.createForClass(Post);