import { Injectable } from '@nestjs/common';
import { Model, Connection, Types } from 'mongoose';
import { InjectModel, InjectConnection } from '@nestjs/mongoose';
import { Post, PostDocument } from '../schemas/post.schema';
import { CreatePost } from '../dto/post.dto';

@Injectable()
export class PostService {
  constructor(
    @InjectModel(Post.name) private postModel: Model<PostDocument>,
    @InjectConnection() private readonly connection: Connection
  ) {}

  async syncData(createPostDto: CreatePost[]): Promise<Post[]> {
    await this.postModel.deleteMany();
    return this.postModel.insertMany(createPostDto);
  }

  async getData(): Promise<Post[]> {
    return this.postModel.find().exec();
  }

  async insertData(createPostDto:CreatePost): Promise<PostDocument> {
    const createdPost = new this.postModel(createPostDto);
    return createdPost.save();
  }

  async deleteData(_id:string): Promise<any> {
    const objectId = new Types.ObjectId(_id);
    return this.postModel.deleteOne({ _id: objectId });
  }
}