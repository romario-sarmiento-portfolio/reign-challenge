import { Test } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';
import { PostService } from './post.service';
import { Post, PostSchema } from '../schemas/post.schema';

describe('PostService', () => {
  let service: PostService;

  beforeAll(async () => {
    const app = await Test.createTestingModule({
      providers: [PostService],
      imports: [
        MongooseModule.forRoot(`mongodb://${process.env.MONGO_DB_HOST}:27017/reign-challenge`),
        MongooseModule.forFeature([{ name: Post.name, schema: PostSchema }])
      ]
    }).compile();

    service = app.get<PostService>(PostService);
  });
  describe('getData', () => {
    it('response', async () => {
      const result:Promise<any> = new Promise((resolve, reject) => resolve);
      jest.spyOn(service, 'insertData').mockImplementation(() => result);
      expect(result).toBeInstanceOf(Promise);
    });
  });
});
