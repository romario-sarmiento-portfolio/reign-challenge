import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { HttpService } from '@nestjs/axios';
import { PostService } from '../services/post.service';

@Injectable()
export class SyncPostsService {
  constructor(
    private httpService: HttpService,
    private readonly postService: PostService
  ) {}
  private readonly logger = new Logger(SyncPostsService.name);

  @Cron(`0 ${process.env.SCHEDULE_MINUTES} * * * *`)
  handleCron() {
    this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').subscribe((response)=>{
      this.postService.syncData(response.data.hits);
    });
  }
}